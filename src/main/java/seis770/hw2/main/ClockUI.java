package seis770.hw2.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ClockUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private ClockPane clockPane;

	private  Container container;
	private JPanel buttonPannels;
	private ButtonPane btnChangeMode;
	private ButtonPane btnIncrement;
	private ButtonPane btnDecrement;
	private ButtonPane btnCancel;

    private SimpleClock clock;

    public ClockUI(String title) {
        super(title);
        setLayout(new BorderLayout());

        container = getContentPane();

        clockPane = new ClockPane();
        container.add(clockPane);

        buttonPannels = new JPanel();

        btnChangeMode = new ButtonPane("Change Mode", 150, 40);
        btnIncrement = new ButtonPane("+", 50, 40);
        btnDecrement = new ButtonPane("-", 50, 40);
        btnCancel = new ButtonPane("Cancel", 100, 40);
        

        buttonPannels.add(btnIncrement, BorderLayout.EAST);
        buttonPannels.add(btnDecrement);
        buttonPannels.add(btnChangeMode);
        buttonPannels.add(btnCancel);

        btnChangeMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clock.changeMode();
            }
        });

        btnIncrement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clock.increment();
            }
        });

        btnDecrement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clock.decrement();
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clock.cancel();
            }
        });

        container.add(buttonPannels, BorderLayout.SOUTH);
        
        disableEditMode();
    }

    public void setHour(int hour) {
        this.clockPane.setHour(hour);
    }

    public void setMinute(int minute) {
        this.clockPane.setMinute(minute);
    }

    public void setSecond(int second) {
        this.clockPane.setSecond(second);
    }
    
    public void enableEditMode() {
        btnIncrement.setVisible(true);
        btnDecrement.setVisible(true);
        btnCancel.setVisible(true);
//        container.repaint();
    }

    public void disableEditMode() {
        btnIncrement.setVisible(false);
        btnDecrement.setVisible(false);
        btnCancel.setVisible(false);
        container.repaint();
    }

    public void setClock(SimpleClock clock) {
        this.clock = clock;
    }
    
    public void highlightHour() {
        this.clockPane.highlightHour();
    }

    public void unHighlightHour() {
        this.clockPane.unHighlightHour();
    }

    public void highlightMinute() {
        this.clockPane.highlightMinute();
    }

    public void unHighlightMinute() {
        this.clockPane.unHighlightMinute();
    }

    public void highlightSecond() {
        this.clockPane.highlightSecond();
    }

    public void unHighlightSecond() {
        this.clockPane.unHighlightSecond();
    }
}
