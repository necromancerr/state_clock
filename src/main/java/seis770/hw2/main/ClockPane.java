package seis770.hw2.main;

import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class ClockPane extends JPanel {

    private static final long serialVersionUID = 1L;
    private DigitPane hour;
    private DigitPane minute;
    private DigitPane second;
    private JLabel[] seperator;

    public ClockPane() {
        setLayout(new GridBagLayout());

        hour = new DigitPane();
        minute = new DigitPane();
        second = new DigitPane();
        seperator = new DigitPane[] { new DigitPane(":"), new DigitPane(":") };

        add(hour);
        add(seperator[0]);
        add(minute);
        add(seperator[1]);
        add(second);

    }

    public void setHourFocus() {
        hour.setFocus();
    }

    public void setMinuteFocus() {
        minute.setFocus();
    }

    public void setSecondFocus() {
        second.setFocus();
    }

    public void setHour(int hour) {
        this.hour.setValue(hour);
    }

    public int getHour() {
        return this.hour.getValue();
    }

    public void setMinute(int minute) {
        this.minute.setValue(minute);
    }

    public int getMinute() {
        return this.minute.getValue();
    }

    public void setSecond(int second) {
        this.second.setValue(second);
    }

    public int getSecond() {
        return this.second.getValue();
    }

    public void highlightHour() {
        this.hour.highlightPane();
    }

    public void unHighlightHour() {
        this.hour.unhighlightPane();
    }

    public void highlightMinute() {
        this.minute.highlightPane();
    }

    public void unHighlightMinute() {
        this.minute.unhighlightPane();
    }

    public void highlightSecond() {
        this.second.highlightPane();
    }

    public void unHighlightSecond() {
        this.second.unhighlightPane();
    }
}
