package seis770.hw2.main;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;

import javax.swing.JFrame;

import seis770.hw2.ClockState.ClockState;
import seis770.hw2.ClockState.ClockStateDisplayTime;

public class SimpleClock {

    private int hour;
    private int minute;
    private int second;


    private ClockState myClockState;
    private static ClockUI myClockUI;
    private Timer simpleClockTimer;

    public SimpleClock() {
        setClockToCurrentTime();
        setMyClockState(ClockStateDisplayTime.getInstance());

        simpleClockTimer = new java.util.Timer();
        simpleClockTimer.schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                myClockState.timerTicked();
            }
        }, 1000, 1000);
    }

    public static void main(String[] args) {

        myClockUI = new ClockUI("Simple Clock");
        myClockUI.setSize(400, 150);
        myClockUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myClockUI.setVisible(true);

        SimpleClock clock = new SimpleClock();
        clock.setMyClockUI(myClockUI);
        myClockUI.setClock(clock);
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public ClockState getMyClockState() {
        return myClockState;
    }

    public void setMyClockState(ClockState newState) {
        this.myClockState = newState;
        this.myClockState.setMySimpleClock(this);
    }

    public ClockUI getMyClockUI() {
        return myClockUI;
    }

    public void setMyClockUI(ClockUI clockUI) {
        myClockUI = clockUI;
    }

    public void increment() {
        myClockState.increment();
    }

    public void decrement() {
        myClockState.decrement();
    }

    public void changeMode() {
        myClockState.changeMode();
    }

    public void cancel() {
        myClockState.cancel();
    }

    public void timerTicked() {
        myClockState.timerTicked();
    }

    public void changeState(ClockState newState) {
        this.myClockState.exitState();
        this.setMyClockState(newState);
        this.myClockState.enterState();
    }

    public void incrementHour() {
        hour++;
        if (hour > 23) {
            hour = 0;
        }
        if (myClockUI != null) {
            myClockUI.setHour(hour);
        }
    }

    public void decrementHour() {
        hour--;
        if (hour < 0) {
            hour = 23;
        }
        if (myClockUI != null) {
            myClockUI.setHour(hour);
        }
    }

    public void incrementMinute() {
        minute++;
        if (minute > 59) {
            minute = 0;
            incrementHour();
        }
        if (myClockUI != null) {
            myClockUI.setMinute(minute);
        }
    }

    public void decrementMinute() {
        minute--;
        if (minute < 0) {
            minute = 59;
        }
        if (myClockUI != null) {
            myClockUI.setMinute(minute);
        }
    }

    public void incrementSecond() {
        second++;
        if (second > 59) {
            second = 0;
            incrementMinute();
        }
        if (myClockUI != null) {
            myClockUI.setSecond(second);
        }
    }

    public void decrementSecond() {
        second--;
        if (second < 0) {
            second = 59;
        }
        if (myClockUI != null) {
            myClockUI.setSecond(second);
        }
    }

    private void setClockToCurrentTime() {
        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        second = calendar.get(Calendar.SECOND);
        
        myClockUI.setHour(hour);
        myClockUI.setMinute(minute);
        myClockUI.setSecond(second);
    }

    public void enterSetHour() {
        myClockUI.enableEditMode();
        myClockUI.highlightHour();
    }

    public void exitSetHour() {
        myClockUI.unHighlightHour();
    }

    public void enterDisplayTime() {
        myClockUI.disableEditMode();
    }

    public void enterSetMinute() {
        myClockUI.enableEditMode();
        myClockUI.highlightMinute();
    }

    public void exitSetMinute() {
        myClockUI.unHighlightMinute();
    }

    public void enterSetSecond() {
        myClockUI.enableEditMode();
        myClockUI.highlightSecond();
    }

    public void exitSetSecond() {
        myClockUI.unHighlightSecond();
    }

}
