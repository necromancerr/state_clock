package seis770.hw2.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;

public class DigitPane extends JLabel {

    private static final long serialVersionUID = 1L;
    private int value;
    private JLabel label;

    public DigitPane() {
        super();
        init();
    }

    public DigitPane(String text) {
        super();
        init();
        setValue(text);
    }

    private void init() {
        label = this;
        label.setFont(new Font(label.getName(), Font.PLAIN, 20));
        setPreferredSize(getPreferredSize());
    }

    public void setValue(int newValue) {
        this.requestFocusInWindow();
        if (value != newValue) {
            value = newValue;
            label.removeAll();
            label.setText(pad(value));
        }
    }

    public void setValue(String newValue) {
        this.requestFocusInWindow();
        label.setText(newValue);
    }

    public int getValue() {
        return value;
    }

    protected String pad(int value) {
        StringBuilder sb = new StringBuilder(String.valueOf(value));
        while (sb.length() < 2) {
            sb.insert(0, "0");
        }
        return sb.toString();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(50, 50);
    }

    public void setFocus() {
        super.requestFocus();
    }

    public void highlightPane() {
        label.setForeground(Color.BLUE);
        label.setFont(new Font(label.getName(), Font.BOLD, 25));
    }

    public void unhighlightPane() {
        label.setForeground(Color.BLACK);
        label.setFont(new Font(label.getName(), Font.PLAIN, 20));
    }
}
