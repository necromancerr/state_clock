package seis770.hw2.main;

import java.awt.Dimension;

import javax.swing.JButton;

public class ButtonPane extends JButton {

    private static final long serialVersionUID = 1L;

    public ButtonPane(String title, int width, int height) {
        super(title);
        setPreferredSize(new Dimension(width, height));
    }
}
