package seis770.hw2.ClockState;

public class ClockStateSetHour extends ClockState {

    private static ClockStateSetHour instance = null;
    
    public static ClockStateSetHour getInstance() {
        if (instance == null) {
            instance = new ClockStateSetHour();
        }
        return instance;
    }
    
    @Override
    public void increment() {
        getMySimpleClock().incrementHour();
    }

    @Override
    public void decrement() {
        getMySimpleClock().decrementHour();;
    }

    @Override
    public void changeMode() {
        getMySimpleClock().changeState(ClockStateSetMinute.getInstance());
    }

    @Override
    public void cancel() {
        getMySimpleClock().changeState(ClockStateDisplayTime.getInstance());
    }

    @Override
    public void enterState() {
        getMySimpleClock().enterSetHour();
    }

    @Override
    public void exitState() {
        getMySimpleClock().exitSetHour();
    }
}
