package seis770.hw2.ClockState;

import seis770.hw2.main.SimpleClock;

public abstract class ClockState {
    private SimpleClock mySimpleClock;

    protected ClockState() {
        
    }

    public SimpleClock getMySimpleClock() {
        return mySimpleClock;
    }

    public void setMySimpleClock(SimpleClock mySimpleClock) {
        this.mySimpleClock = mySimpleClock;
    }

    public abstract void increment();

    public abstract void decrement();

    public abstract void changeMode();

    public abstract void cancel();

    public void timerTicked() {
        this.getMySimpleClock().incrementSecond();
    }

    public abstract void enterState();

    public abstract void exitState();
}
