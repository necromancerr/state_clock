package seis770.hw2.ClockState;

public class ClockStateDisplayTime extends ClockState {
    private static ClockStateDisplayTime instance = null;
    
    public static ClockStateDisplayTime getInstance() {
        if (instance == null) {
            instance = new ClockStateDisplayTime();
        }
        return instance;
    }

    @Override
    public void increment() {
        
    }

    @Override
    public void decrement() {
        
    }

    @Override
    public void changeMode() {
        getMySimpleClock().changeState(ClockStateSetHour.getInstance());
    }

    @Override
    public void cancel() {
        
    }

    @Override
    public void enterState() {
        getMySimpleClock().enterDisplayTime();
    }

    @Override
    public void exitState() {
        // TODO Auto-generated method stub
        
    }

}
