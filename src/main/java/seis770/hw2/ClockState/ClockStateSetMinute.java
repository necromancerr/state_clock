package seis770.hw2.ClockState;

public class ClockStateSetMinute extends ClockState {

    private static ClockStateSetMinute instance = null;
    
    public static ClockStateSetMinute getInstance() {
        if (instance == null) {
            instance = new ClockStateSetMinute();
        }
        return instance;
    }

    @Override
    public void increment() {
        getMySimpleClock().incrementMinute();
    }

    @Override
    public void decrement() {
        getMySimpleClock().decrementMinute();
    }

    @Override
    public void changeMode() {
        getMySimpleClock().changeState(ClockStateSetSecond.getInstance());
    }

    @Override
    public void cancel() {
        getMySimpleClock().changeState(ClockStateSetHour.getInstance());
    }

    @Override
    public void enterState() {
        getMySimpleClock().enterSetMinute();
    }

    @Override
    public void exitState() {
        getMySimpleClock().exitSetMinute();
    }

}
