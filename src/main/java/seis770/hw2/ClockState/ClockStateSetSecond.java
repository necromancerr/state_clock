package seis770.hw2.ClockState;

public class ClockStateSetSecond extends ClockState {

    private static ClockStateSetSecond instance = null;
    
    public static ClockStateSetSecond getInstance() {
        if (instance == null) {
            instance = new ClockStateSetSecond();
        }
        return instance;
    }

    @Override
    public void increment() {
        getMySimpleClock().incrementSecond();
    }

    @Override
    public void decrement() {
        getMySimpleClock().decrementSecond();
    }

    @Override
    public void changeMode() {
        getMySimpleClock().changeState(ClockStateDisplayTime.getInstance());
    }

    @Override
    public void cancel() {
        getMySimpleClock().changeState(ClockStateSetMinute.getInstance());
    }
    
    @Override
    public void timerTicked() {
        
    }

    @Override
    public void enterState() {
        getMySimpleClock().enterSetSecond();
    }

    @Override
    public void exitState() {
        getMySimpleClock().exitSetSecond();
    }

}
